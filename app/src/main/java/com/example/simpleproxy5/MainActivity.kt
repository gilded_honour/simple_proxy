package com.example.simpleproxy5

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.InetSocketAddress
import java.net.ServerSocket
import java.net.Socket
import java.util.Properties
import java.util.concurrent.atomic.AtomicLong

class MainActivity : AppCompatActivity() {
    companion object {
        const val LOGIN_LENGTH = 8
        const val PASSWORD_LENGTH = 8
    }

    private lateinit var serverThread: Thread
    private lateinit var logEditText: EditText
    private lateinit var txtRx: TextView
    private lateinit var txtTx: TextView
    private val rxAtomic = AtomicLong(0)
    private val txAtomic = AtomicLong(0)
    private lateinit var handler: Handler

    private lateinit var config: Properties
    private lateinit var destinationServerHost: String
    //must be String otherwise, with Int, it won't compile
    private lateinit var destinationServerPort: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        readConfig()

        logEditText = findViewById(R.id.logEditText)
        txtRx = findViewById(R.id.rxTextView)
        txtTx = findViewById(R.id.txTextView)
        handler = Handler(Looper.getMainLooper())

        serverThread = Thread {
            startServer()
        }
        serverThread.start()
    }

    private fun readConfig() {
        config = Properties()
        try {
            val rawResource = resources.openRawResource(R.raw.config)
            config.load(rawResource)
        } catch (e: IOException) {
            logToUI("Error reading config: ${e.message}")
        }
    }

    private fun startServer() {
        try {
            //for IPv6
            //  val serverSocket = ServerSocket(8282)

            //for IPv4 only
            val localAddress = config.getProperty("local_address", "0.0.0.0")
            val localPort = config.getProperty("local_port", "8080").toInt()
            val address = InetSocketAddress(localAddress, localPort)
            val serverSocket = ServerSocket()
            serverSocket.bind(address)

            logToUI("server listens on ${serverSocket.localSocketAddress}")

            destinationServerHost = config.getProperty("destination_server_host", "")
            destinationServerPort = config.getProperty("destination_server_port", "8080")

            while (true) {
                val clientSocket = serverSocket.accept()
                val clientHandler = ClientHandler(clientSocket)
                Thread(clientHandler).start()
            }

        } catch (e: IOException) {
            logToUI("error: $e")
        }
    }

    inner class ClientHandler(private val clientSocket: Socket) : Runnable {
        override fun run() {
            try {
                logToUI("Client connected: ${clientSocket.inetAddress.hostAddress}")
                val clientReader = BufferedReader(InputStreamReader(clientSocket.getInputStream()))
                val maybeCredentials = clientReader.readLine()
                if (authenticate(maybeCredentials)) {
                    logToUI("authentication successful")

                    val destinationServerSocket = Socket(destinationServerHost, destinationServerPort.toInt())
                    val destinationServerWriter = PrintWriter(destinationServerSocket.getOutputStream(), true)
                    val destinationServerReader = BufferedReader(InputStreamReader(destinationServerSocket.getInputStream()))
                    val clientWriter = PrintWriter(clientSocket.getOutputStream(), true)
                    var message: String?

                    while (true) {
                        message = clientReader.readLine()
                        if (message == null || message == "exit") {
                            break
                        }

                        logToUI("Client(${clientSocket.inetAddress.hostAddress}): ${message.take(10)}...")
                        destinationServerWriter.println(message)
                        rxAtomic.addAndGet(message.length.toLong())

                        val response = destinationServerReader.readLine()
                        logToUI("Destination Server: ${response.take(10)}...")
                        clientWriter.println(response)
                        txAtomic.addAndGet(response.length.toLong())
                    }

                    destinationServerSocket.close()

                    logToUI("Client disconnected: ${clientSocket.inetAddress.hostAddress}")
                    logToUI("---\r\n")
                } else {
                    logToUI("authentication failed")
                    logToUI("---\r\n")
                }

                clientSocket.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        private fun authenticate(credentials: String): Boolean {
            return try {
                val (userLogin, userPassword) = credentials.split(":")

                //TODO - mandatory to verify the length?
                //login-password is sent as the 1st line
                //if (userLogin.length == LOGIN_LENGTH && userPassword.length == PASSWORD_LENGTH) {
                if (true) {
                    val a1 = userLogin == config.getProperty("login")
                    val a2 = userPassword == config.getProperty("password")
                    a1 && a2
                } else {
                    false
                }
            } catch (e: Exception) {
                false
            }
        }
    }

    private fun logToUI(message: String) {
        handler.post {
            logEditText.append("$message\n")
            txtRx.text = "Rx: ${rxAtomic.get()}"
            txtTx.text = "Tx: ${txAtomic.get()}"
        }
    }
}
